# frozen_string_literal: true

require 'require_all'
require 'singleton'
require_rel 'c4/model'
require_rel 'c4/view'
require_rel 'c4/version.rb'

module C4
  class App
    include Singleton

    attr_reader :game, :viewer

    def initialize
      @game = C4::Model::Game.new
      @viewer = C4::View::Cli.new(game)
    end

    def run
      start
      finish
    end

    private

    def start
      until game.winner || game.impasse?
        viewer.print
        play
      end

      viewer.print
    end

    def play
      puts "Player \"#{game.current_player}\" - choose column (type a number from 0 to #{C4::Model::Game::COLUMNS}): "
      column = STDIN.gets.chomp.to_i
      game.play!(column)
    rescue C4::Model::ColumnFullError, C4::Model::InvalidColumnError => e
      puts e.message
      retry
    end

    def finish
      return puts("Player \"#{game.winner}\" won!") if game.winner
      return puts('No more moves possible. Game over.') if game.impasse?
    end
  end
end
