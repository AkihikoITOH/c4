# frozen_string_literal: true

module C4
  module View
    class Cli
      attr_reader :game

      def initialize(game)
        @game = game
      end

      def print
        puts text
      end

      private

      def text
        [text_columns,
         text_divider,
         text_column_numbers].join("\n") + "\n"
      end

      def board
        game.board.to_a(:print)
      end

      def printable_board
        board.map do |lv|
          lv.map do |e|
            e.nil? ? ' ' : e
          end
        end
      end

      def text_columns
        printable_board.map do |lv|
          "|#{lv.join('|')}|"
        end.join("\n")
      end

      def text_divider
        '-' * (printable_board.first.size * 2 + 1)
      end

      def text_column_numbers
        "|#{(0...printable_board.first.size).to_a.join('|')}|"
      end
    end
  end
end
