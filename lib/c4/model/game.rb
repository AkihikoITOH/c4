# frozen_string_literal: true

require_relative 'board'
require_relative 'errors'
require_relative 'refinement/streak'

module C4
  module Model
    class Game
      using Refinement::Streak

      attr_reader :board, :players, :current_player_index

      P1 = 'o'
      P2 = 'x'
      ROWS = 6
      COLUMNS = 7
      WINNING_LENGTH = 4

      def initialize
        @board = Board.new(ROWS, COLUMNS)
        @players = [P1, P2]
        @current_player_index = 0
      end

      def play!(column)
        board.put_stone!(column, current_player)
        toggle_player
      end

      def current_player
        players[current_player_index]
      end

      def impasse?
        board.full?
      end

      def winner
        players.find do |player|
          player if valid_streak_exists?(player)
        end
      end

      private

      def toggle_player
        @current_player_index = (current_player_index + 1) % 2
      end

      def valid_streak_exists?(player)
        vertical_streak_exists?(player) || horizontal_streak_exists?(player) || diagonal_streak_exists?(player)
      end

      def vertical_streak_exists?(player)
        board.to_a(:column_wise).any? do |column|
          column.streak?(player, WINNING_LENGTH)
        end
      end

      def horizontal_streak_exists?(player)
        board.to_a(:row_wise).any? do |row|
          row.streak?(player, WINNING_LENGTH)
        end
      end

      def diagonal_streak_exists?(player)
        board.diagonals.any? do |row|
          row.streak?(player, WINNING_LENGTH)
        end
      end
    end
  end
end
