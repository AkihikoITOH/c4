# frozen_string_literal: true

module C4
  module Model
    module Refinement
      module Filler
        refine Array do
          # fill_up_to fills the array by appending the given value until the size reaches the disired size.
          def fill_up_to(desired_size, value = nil)
            return self if size >= desired_size

            self + Array.new(desired_size - size, value)
          end
        end
      end
    end
  end
end
