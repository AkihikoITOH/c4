# frozen_string_literal: true

module C4
  module Model
    module Refinement
      module Streak
        refine Array do
          # streak? looks for a streak of the given mark of the given length.
          # Returns true if it finds at least one streak.
          def streak?(value, streak_length)
            return false if streak_length <= 0 || streak_length > length

            (0..(length - streak_length)).any? do |idx|
              self[idx, streak_length].all? { |element| element == value }
            end
          end
        end
      end
    end
  end
end
