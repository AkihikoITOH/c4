# frozen_string_literal: true

require_relative 'errors'
require_relative 'refinement/filler'

module C4
  module Model
    class Column
      using Refinement::Filler

      attr_reader :stones, :max_size

      def initialize(max_size)
        @stones = []
        @max_size = max_size
      end

      def put!(mark)
        raise ColumnFullError, 'This column is full!' if full?

        stones.push(mark)
      end

      def full?
        stones.size >= max_size
      end

      def to_a
        stones.fill_up_to(max_size, nil)
      end
    end
  end
end
