# frozen_string_literal: true

require_relative 'refinement/filler'

module C4
  module Model
    class Matrix
      using Refinement::Filler

      attr_reader :matrix, :rows, :columns

      def initialize(matrix, rows, columns)
        @matrix = matrix
        @rows = rows
        @columns = columns
      end

      def transpose
        self.class.new(
          Array.new(rows).each_with_index.map do |_, row|
            Array.new(columns).each_with_index.map do |_, column|
              matrix[column][row]
            end
          end,
          columns,
          rows
        )
      end

      def size
        [rows, columns].max
      end

      def partial_square(from, to)
        self.class.new(matrix[from..to].map { |a| a[from..to] }, to - from + 1, to - from + 1)
      end

      # rubocop:disable Metrics/AbcSize
      def square
        sq = matrix.map do |col|
          col[0, size].fill_up_to(size, nil)
        end
        sq[0, size].fill_up_to(size, Array.new(size))
        self.class.new(sq, size, size)
      end
      # rubocop:enable Metrics/AbcSize

      def diagonal
        Array.new(size).each_with_index.map do |_, i|
          matrix[i][i]
        end
      end

      def reverse
        self.class.new(matrix.reverse, rows, columns)
      end

      # rubocop:disable Metrics/AbcSize, Performance/TimesMap
      def rotate
        result = rows.times.map do |r|
          columns.times.map do |c|
            matrix[columns - c - 1][r]
          end
        end
        self.class.new(result, columns, rows)
      end
      # rubocop:enable Metrics/AbcSize, Performance/TimesMap

      def diagonals
        quadrants.flat_map(&:quadrant_diagonals).uniq
      end

      def quadrants
        [
          square,
          square.rotate,
          square.rotate.rotate,
          square.rotate.rotate.rotate
        ]
      end

      def quadrant_diagonals
        (0...size).map do |i|
          partial_square(0, i).rotate.diagonal
        end
      end
    end
  end
end
