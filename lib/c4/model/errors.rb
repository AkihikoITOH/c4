# frozen_string_literal: true

module C4
  module Model
    class ColumnFullError < StandardError; end
    class InvalidColumnError < StandardError; end
  end
end
