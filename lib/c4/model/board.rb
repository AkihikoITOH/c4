# frozen_string_literal: true

require_relative 'column'
require_relative 'matrix'

module C4
  module Model
    class Board
      attr_reader :columns, :num_rows, :num_columns

      using Refinement::Filler

      def initialize(num_rows, num_columns)
        @num_rows = num_rows
        @num_columns = num_columns
        @columns = Array.new(num_columns).map { |_| Column.new(num_rows) }
      end

      def put_stone!(column, mark)
        raise InvalidColumnError, 'Column does not exist!' if column.negative? || column >= columns.size

        columns[column].put!(mark)
      end

      def full?
        columns.all?(&:full?)
      end

      def to_a(mode = :column_wise)
        column_wise = Matrix.new(columns.map(&:to_a), num_rows, num_columns)
        return column_wise.transpose.matrix if mode == :row_wise # Changes the shape
        return column_wise.transpose.reverse.matrix if mode == :print

        column_wise.matrix
      end

      def diagonals
        matrix = Matrix.new(columns.map(&:to_a), num_rows, num_columns)
        matrix.diagonals
      end
    end
  end
end
