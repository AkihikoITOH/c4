# frozen_string_literal: true

RSpec.describe C4::Model::Game do
  subject { game }

  let(:game) { described_class.new }

  describe '#play!' do
    subject { game.play!(column) }

    let(:column) { rand(described_class::COLUMNS) }

    before do
      allow(game).to receive(:current_player).and_return(described_class::P1)
      allow(game.board).to receive(:put_stone!).with(column, described_class::P1).and_call_original
      allow(game).to receive(:toggle_player).and_call_original
      subject
    end

    it 'puts stone' do
      expect(game.board).to have_received(:put_stone!).with(column, described_class::P1).once
    end
    it 'toggles player' do
      expect(game).to have_received(:toggle_player).once
    end
  end

  describe '#current_player' do
    subject { game.current_player }

    context 'when initialized' do
      it { is_expected.to eq described_class::P1 }
    end

    context 'after one play' do
      before { game.play!(column) }

      let(:column) { rand(described_class::COLUMNS) }

      it { is_expected.to eq described_class::P2 }

      context 'after another play' do
        before { game.play!(column) }

        it { is_expected.to eq described_class::P1 }
      end
    end
  end

  describe '#impasse?' do
    subject { game.impasse? }

    let(:board) { double(:board, full?: false) }

    before do
      allow(game).to receive(:board).and_return(board)
      subject
    end

    it 'asks the underlying board' do
      expect(board).to have_received(:full?).once
    end
  end

  describe '#winner' do
    subject { game.winner }

    context 'there is a winner' do
      before do
        allow(game).to receive(:valid_streak_exists?).with(game.players.first).and_return(false)
        allow(game).to receive(:valid_streak_exists?).with(game.players.last).and_return(true)
      end

      it 'returns the winner' do
        is_expected.to eq game.players.last
      end
    end

    context 'there is not a winner yet' do
      before do
        allow(game).to receive(:valid_streak_exists?).with(anything).and_return(false)
      end

      it 'returns the winner' do
        is_expected.to be_nil
      end
    end
  end

  describe '#valid_streak_exists?' do
    subject { game.send(:valid_streak_exists?, player) }

    let(:o) { described_class::P1 }
    let(:x) { described_class::P2 }
    let(:n) { nil }

    before do
      allow(game.board).to receive(:columns).and_return(situation)
    end

    context 'just started' do
      let(:situation) do
        Array.new(described_class::COLUMNS, Array.new(described_class::ROWS, n))
      end

      context 'P1' do
        let(:player) { o }

        it { is_expected.to be_falsey }
      end

      context 'P2' do
        let(:player) { x }

        it { is_expected.to be_falsey }
      end
    end

    context 'no path yet' do
      let(:situation) do
        [
          [n, n, n, n, n, n],
          [o, x, x, n, n, n],
          [x, o, n, n, n, n],
          [o, o, x, o, o, n],
          [o, o, x, x, n, n],
          [o, x, x, n, n, n],
          [x, n, n, n, n, n]
        ]
      end

      context 'P1' do
        let(:player) { o }

        it { is_expected.to be_falsey }
      end

      context 'P2' do
        let(:player) { x }

        it { is_expected.to be_falsey }
      end
    end

    context 'vertical path' do
      let(:situation) do
        [
          [x, x, n, n, n, n],
          [o, x, x, x, n, n],
          [x, o, o, o, o, n],
          [o, o, x, o, o, n],
          [o, o, x, x, x, n],
          [o, x, x, n, n, n],
          [x, n, n, n, n, n]
        ]
      end

      context 'P1' do
        let(:player) { o }

        it { is_expected.to be_truthy }
      end

      context 'P2' do
        let(:player) { x }

        it { is_expected.to be_falsey }
      end
    end

    context 'horizontal path' do
      let(:situation) do
        [
          [n, n, n, n, n, n],
          [o, x, o, n, n, n],
          [x, o, x, n, n, n],
          [o, o, x, o, o, n],
          [o, o, x, x, n, n],
          [o, x, x, n, n, n],
          [x, x, n, n, n, n]
        ]
      end

      context 'P1' do
        let(:player) { o }

        it { is_expected.to be_falsey }
      end

      context 'P2' do
        let(:player) { x }

        it { is_expected.to be_truthy }
      end
    end

    context 'diagonal path 1' do
      let(:situation) do
        [
          [x, n, n, n, n, n],
          [o, x, x, o, n, n],
          [x, o, o, n, n, n],
          [o, o, x, o, o, n],
          [o, o, x, x, n, n],
          [o, x, x, n, n, n],
          [x, x, n, n, n, n]
        ]
      end

      context 'P1' do
        let(:player) { o }

        it { is_expected.to be_truthy }
      end

      context 'P2' do
        let(:player) { x }

        it { is_expected.to be_falsey }
      end
    end

    context 'diagonal path 2' do
      let(:situation) do
        [
          [x, n, n, n, n, n],
          [o, x, x, n, n, n],
          [x, o, o, n, n, n],
          [o, o, x, o, o, n],
          [o, o, x, x, o, n],
          [o, x, x, o, x, o],
          [x, x, n, n, n, n]
        ]
      end

      context 'P1' do
        let(:player) { o }

        it { is_expected.to be_truthy }
      end

      context 'P2' do
        let(:player) { x }

        it { is_expected.to be_falsey }
      end
    end
  end
end
