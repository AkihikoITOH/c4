# frozen_string_literal: true

RSpec.describe C4::Model::Board do
  subject { board }

  let(:size_rows) { 6 }
  let(:size_columns) { 7 }
  let(:board) { described_class.new(size_rows, size_columns) }

  describe '#put_stone!' do
    subject { board.put_stone!(put_column, mark) }

    let(:columns) { Array.new(size_columns).map { |_| double(put!: []) } }
    let(:put_column) { rand(7) }
    let(:mark) { 'x' }

    before do
      allow(board).to receive(:columns).and_return(columns)
      subject
    end

    it { expect(columns[put_column]).to have_received(:put!).with(mark) }
  end

  describe '#full?' do
    subject { board.full? }

    let(:columns) { Array.new(size_columns).map { |_| double(full?: false) } }

    before { allow(board).to receive(:columns).and_return(columns) }

    it { is_expected.to eq false }

    context 'when one column is full' do
      before { columns[rand(7)] = double(full?: true) }

      it { is_expected.to eq false }
    end

    context 'when all columns are full' do
      let(:columns) { Array.new(size_columns).map { |_| double(full?: true) } }

      it { is_expected.to eq true }
    end
  end

  describe '#to_a' do
    subject { board.to_a(mode) }

    let(:board) { described_class.new(3, 3) }
    let(:o) { 'o' }
    let(:x) { 'x' }
    let(:n) { nil }
    let(:columns) do
      [
        [x, n, n],
        [x, o, n],
        [o, o, x]
      ]
    end

    before do
      allow(board).to receive(:columns).and_return(columns)
    end

    context 'column wise' do
      let(:mode) { :column_wise }
      let(:result) do
        [
          [x, n, n],
          [x, o, n],
          [o, o, x]
        ]
      end

      it { is_expected.to eq result }
    end

    context 'row wise' do
      let(:mode) { :row_wise }
      let(:result) do
        [
          [x, x, o],
          [n, o, o],
          [n, n, x]
        ]
      end

      it { is_expected.to eq result }
    end

    context 'print' do
      let(:mode) { :print }
      let(:result) do
        [
          [n, n, x],
          [n, o, o],
          [x, x, o]
        ]
      end

      it { is_expected.to eq result }
    end
  end
end
