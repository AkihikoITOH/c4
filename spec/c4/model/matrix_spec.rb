# frozen_string_literal: true

RSpec.describe C4::Model::Matrix do
  subject { matrix }

  let(:matrix) { described_class.new(array, rows, columns) }

  describe '#diagonals' do
    subject { matrix.diagonals }

    let(:rows) { 3 }
    let(:columns) { 3 }
    let(:array) do
      [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ]
    end
    let(:diagonals) do
      [
        [1], [4, 2], [7, 5, 3],
        [7], [8, 4], [9, 5, 1],
        [9], [6, 8], [3, 5, 7],
        [3], [2, 6], [1, 5, 9]
      ]
    end

    it { is_expected.to eq diagonals }
  end

  describe '#partial_square' do
    subject { matrix.partial_square(1, 3).matrix }

    let(:rows) { 4 }
    let(:columns) { 5 }
    let(:array) do
      [
        [1, 2, 3, 4],
        [5, 6, 7, 8],
        [9, 10, 11, 12],
        [13, 14, 15, 16],
        [17, 18, 19, 20]
      ]
    end
    let(:partial) do
      [
        [6, 7, 8],
        [10, 11, 12],
        [14, 15, 16]
      ]
    end

    it { is_expected.to eq partial }
  end

  describe '#rotate' do
    subject { matrix.rotate.matrix }

    let(:rows) { 2 }
    let(:columns) { 3 }
    let(:array) do
      [
        [1, 2],
        [3, 4],
        [5, 6]
      ]
    end
    let(:rotate) do
      [
        [5, 3, 1],
        [6, 4, 2]
      ]
    end

    it { is_expected.to eq rotate }

    context '2x' do
      subject { matrix.rotate.rotate.matrix }

      let(:rotate) do
        [
          [6, 5],
          [4, 3],
          [2, 1]
        ]
      end

      it { is_expected.to eq rotate }
    end

    context '3x' do
      subject { matrix.rotate.rotate.rotate.matrix }

      let(:rotate) do
        [
          [2, 4, 6],
          [1, 3, 5]
        ]
      end

      it { is_expected.to eq rotate }
    end
  end

  describe '#transpose' do
    subject { matrix.transpose.matrix }

    let(:rows) { 2 }
    let(:columns) { 3 }
    let(:array) do
      [
        [1, 2],
        [4, 5],
        [7, 8]
      ]
    end
    let(:transpose) do
      [
        [1, 4, 7],
        [2, 5, 8]
      ]
    end

    it { is_expected.to eq transpose }

    context 'twice' do
      subject { matrix.transpose.transpose.matrix }

      it { is_expected.to eq array }
    end
  end

  describe '#square' do
    subject { matrix.square.matrix }

    let(:rows) { 2 }
    let(:columns) { 3 }
    let(:array) do
      [
        [1, 2],
        [4, 5],
        [7, 8]
      ]
    end
    let(:square) do
      [
        [1, 2, nil],
        [4, 5, nil],
        [7, 8, nil]
      ]
    end

    it { is_expected.to eq square }
  end

  describe '#diagonal' do
    subject { matrix.diagonal }

    let(:rows) { 3 }
    let(:columns) { 3 }
    let(:array) do
      [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ]
    end

    it { is_expected.to eq [1, 5, 9] }
  end

  describe '#reverse' do
    subject { matrix.reverse.matrix }

    let(:rows) { 3 }
    let(:columns) { 3 }
    let(:array) do
      [
        [1, 2, 3],
        [4, 5, 6],
        [7, 8, 9]
      ]
    end
    let(:reverse) do
      [
        [7, 8, 9],
        [4, 5, 6],
        [1, 2, 3]
      ]
    end

    it { is_expected.to eq reverse }
  end
end
