# frozen_string_literal: true

RSpec.describe C4::Model::Column do
  subject { column }

  let(:size) { 6 }
  let(:mark) { 'x' }
  let(:prefilled) { rand(size - 1) }
  let(:column) { described_class.new(size) }

  describe '#put!' do
    subject { -> { column.put!(mark) } }

    it { is_expected.to change { column.stones.size }.by(1) }

    context 'when it is full' do
      before do
        size.times { |_| column.put!(mark) }
      end

      it { is_expected.to raise_error(C4::Model::ColumnFullError) }
    end
  end

  describe '#full?' do
    subject { column.full? }

    before do
      prefilled.times { |_| column.put!(mark) }
    end

    it { is_expected.to be_falsey }

    context 'when it is full' do
      let(:prefilled) { size }

      it { is_expected.to be_truthy }
    end
  end

  describe '#to_a' do
    subject { column.to_a }

    it { is_expected.to be_an Array }
    it { expect(subject.size).to eq size }
  end
end
