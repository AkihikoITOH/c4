# frozen_string_literal: true

RSpec.describe C4::View::Cli do
  subject { view }

  let(:game) { C4::Model::Game.new }
  let(:view) { described_class.new(game) }

  describe '#print' do
    subject { view.print }

    let(:board_text) do
      <<~TEXT
        | | | | | | | |
        | | | | | | | |
        | | | | | | | |
        | | | | | | | |
        | | | | | | | |
        | | | | | | | |
        ---------------
        |0|1|2|3|4|5|6|
      TEXT
    end

    before do
      allow(STDOUT).to receive(:puts).with(board_text).and_return(nil)
      subject
    end

    it 'prints the board' do
      expect(STDOUT).to have_received(:puts).with(board_text).once
    end
  end
end
